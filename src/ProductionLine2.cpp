/*
 * File:   Production1.cpp
 * Author: ondrej
 *
 * Created on 30. listopad 2013, 14:16
 */

#include "ProductionLine2.h"
#include "constants.h"
#include "Product.h"
#include "CombinedStorage.h"

// Production line 1 initialization
ProductionLine2 *line2;

ProductionLine2::ProductionLine2() {
    int i;

    // inicializace linek a zasobniku
    line.SetCapacity(PRODUCTION_2_LINE_COUNT);
    line.SetName("Linka 2");

    componentV1 = &storageV1;
    componentB = (*internalStorage)[CombinedStorage::COMPONENT_B];
    componentC = (*internalStorage)[CombinedStorage::COMPONENT_C];
    componentE = (*internalStorage)[CombinedStorage::COMPONENT_E];
}

void ProductionLine2::EnterLine(Product* p) {
    line.Enter(p, 1);
}

void ProductionLine2::LeaveLine(Product *p) {
    line.Leave(1);
}

void ProductionLine2::DoProduction(Product* p) {

    // Take components
    componentV1->fetch();
    componentB->fetch();
    componentC->fetch();
    componentE->fetch();

    double t = Time;

    p->componentsUsed[CombinedStorage::COMPONENT_B] += 1;
    p->componentsUsed[CombinedStorage::COMPONENT_C] += 1;
    p->componentsUsed[CombinedStorage::COMPONENT_E] += 1;

    p->Wait(PRODUCTION_2_PRODUCTION_TIME);
    p->productionTime += Time - t;

    storageV2.store();
}