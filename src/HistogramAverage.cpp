/*
 * File:   HistogramAverage.cpp
 * Author: ondrej
 *
 * Created on 9. prosinec 2013, 13:05
 */

#include "HistogramAverage.h"

HistogramAverage::HistogramAverage(const char* _name, double low, double step, unsigned count, unsigned divide) : Histogram(_name, low, step, count) {
    div = divide;
}

void HistogramAverage::Output() {
    Print("+----------------------------------------------------------+\n");
    Print("| HISTOGRAM %-46s |\n", Name());
    //Print("+----------------------------------------------------------+\n");
    stat.Output();
    long sum = 0;
    unsigned i;
    for (i = 0; i <= count + 1; i++)
        sum += dptr[i];
    if (sum == 0) return;
    //Print("+------------+------------+----------+----------+----------+\n");
    Print("|    from    |     to     |     n    |   rel    |   sum    |\n");
    Print("+------------+------------+----------+----------+----------+\n");
    double from, to;
    long s;
    from = low;
    s = dptr[0];
    for (i = 1; i <= count; i++) {
        unsigned x = dptr[i] / div;
        s += x;
        to = from + step;
        Print("| %10.3f | %10.3f | %8u | %8.6f | %8.6f |\n",
                from, to, x, (double) x / sum, (double) s / sum);
        from = to;
    }
    Print("+------------+------------+----------+----------+----------+\n");
    Print("\n");
}