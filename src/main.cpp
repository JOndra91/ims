#include <cstdlib>
#include <iostream>
#include <stdio.h>
#include "simlib.h"
#include "constants.h"
#include "Calendar.h"
#include "IncomingOrder.h"
#include "Customer.h"
#include "Product.h"
#include "ProductionLine1.h"
#include "ProductionLine2.h"
#include "ProductionLine3.h"
#include "TestLine.h"
#include "LineFailure1.h"
#include "HistogramAverage.h"
#include "IncomingDelivery.h"

void init();

int main(int argc, char** argv) {
    int i;
    double start = 0;
    double end = NUMBER_OF_MONTH * 30 * Calendar::DAY; // ~= 3 years

    SetOutput("doc/statistiky.txt");

    std::cout << "SHO - Vyrobni linka" << std::endl;
    std::cout << "Casove obdobi: " << start << " - " << end << " dni ( ~= 3 roky)" << std::endl;

    Print("SHO - Vyrobni linka\n");
    Print("Casove obdobi: %f - %f dni ( ~= 3 roky)\n", start, end);


    Init(start, end);
    init();

    calendar.Activate();

    Run(); // simulation

    //    internalStorage->Output();
    //    storageV1.Output();
    //    storageV2.Output();
    //    storageTest.Output();
    line1->lineStat.Output();
    testLine->heatChamber.Output();
    testLine->stat.Output();
    //    storageFinal.Output();
    IncomingDelivery::inspector.Output();
    line2->line.Output();
    line3->line.Output();
    customerQueue.Output();
    histogramProductsPerDay->Output();

    Print("\n");
    Print("Final Number of ordered products >>> %lu <<<\n", Product::products - Product::faultyProducts);
    Print("Final Number of generated products >>> %lu <<<\n", Product::products);
    Print("Final Number of faulty products >>> %lu <<<\n", Product::faultyProducts);
    return 0;
}

void init() {
    // Internal storage initialization
    internalStorage = new CombinedStorage("Internal storage", INTERNAL_STORAGE_CAPACITY);

    line1 = new ProductionLine1();
    line2 = new ProductionLine2();
    line3 = new ProductionLine3();
    testLine = new TestLine();

    /* Production time */
    productHistogram = new Histogram("Production time",
            0.0 * Calendar::HOUR,
            5.0 * Calendar::HOUR,
            15);

    /* Line 1 Time */
    histogramInLine1 = new Histogram("IN Line 1",
            0.0 * Calendar::HOUR,
            0.08 * Calendar::HOUR,
            20);

    /* Line 2 Time */
    histogramInLine2 = new Histogram("IN Line 2",
            0.0 * Calendar::HOUR,
            0.08 * Calendar::HOUR,
            20);

    /* Line 3 Time */
    histogramInLine3 = new Histogram("IN Line 3",
            0.0 * Calendar::HOUR,
            0.08 * Calendar::HOUR,
            20);

    /* Line 4 Time */
    histogramInLine4 = new Histogram("IN Line 4",
            0.0 * Calendar::HOUR,
            0.08 * Calendar::HOUR,
            20);

    /* Line 4 Chamber Time */
    histogramInLine4Chamber = new Histogram("IN Line 4 Chamber",
            1.8 * Calendar::HOUR,
            0.1 * Calendar::HOUR,
            20);

    /* Products per day */
    histogramProductsPerDay = new HistogramAverage("Products per day in month", 1, 1, 30 /* 30 dni */, NUMBER_OF_MONTH /* 36 ~= 3 roky */);

    /* Production time */
    histogramProductionTime = new Histogram("Production time",
            0, // 0
            0.3, // 0.3
            20); // 20
}