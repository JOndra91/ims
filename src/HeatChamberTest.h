/*
 * File:   HeatChamberTest.h
 * Author: ondrej
 *
 * Created on 5. prosinec 2013, 14:39
 */

#ifndef HEATCHAMBERTEST_H
#define	HEATCHAMBERTEST_H

class HeatChamberTest;

#include "simlib.h"

class HeatChamberTest : public Process {
public:
    void Behavior();
};

#endif	/* HEATCHAMBERTEST_H */

