/*
 * File:   Calendar.cpp
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 16:49
 */

#include "Calendar.h"
#include "IncomingOrderGenerator.h"
#include "Product.h"
#include <cstdlib>
#include <iostream>

Calendar calendar;

unsigned long Calendar::tempProducts = 0;

Calendar::Calendar(Priority_t p) : Process(p) {
    SetName("Kalendar");
    month = 1;
    day = 1;
    phase = 1;
}

void Calendar::Behavior() {

    while (true) {

        if (day == 1 && phase == 1) {
            (new IncomingOrderGenerator)->Activate();
            std::cout << "Mesic " << month << std::endl;
        }

        Wait(Calendar::HOUR * 8.0);
        phase = (phase % 3) + 1;

        if (phase == 1) {
            day = (day % 30) + 1;
            Product::productsToday = 0;
        }

        if (phase == 1 && day == 1) {
            month++;
        }
    }
}