/*
 * File:   ProductionLine3.cpp
 * Author: ondrej
 *
 * Created on 4. prosinec 2013, 20:55
 */

#include "ProductionLine3.h"
#include "constants.h"
#include "Product.h"
#include "CombinedStorage.h"

// Production line 3
ProductionLine3 *line3;

ProductionLine3::ProductionLine3() {

    // inicializace linek a zasobniku
    line.SetCapacity(PRODUCTION_3_LINE_COUNT);
    line.SetName("Linka 3");

    componentV2 = &storageV2;
    componentD = (*internalStorage)[CombinedStorage::COMPONENT_D];
}

void ProductionLine3::EnterLine(Product* p) {
    line.Enter(p, 1);
}

void ProductionLine3::LeaveLine(Product *p) {
    line.Leave(1);
}

void ProductionLine3::DoProduction(Product* p) {

    // Take components
    componentV2->fetch();
    componentD->fetch();

    p->componentsUsed[CombinedStorage::COMPONENT_D] += 1;

    double t = Time;
    p->Wait(PRODUCTION_3_PRODUCTION_TIME);
    p->productionTime += Time - t;
}
