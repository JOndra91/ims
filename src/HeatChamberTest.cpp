/*
 * File:   HeatChamberTest.cpp
 * Author: ondrej
 *
 * Created on 5. prosinec 2013, 14:39
 */

#include "HeatChamberTest.h"
#include "TestLine.h"
#include "constants.h"

void HeatChamberTest::Behavior() {
    Product *prod;

    testLine->EnterTestLine(this, 2);
    testLine->DoHeatChamberSetup(this);
    testLine->LeaveTestLine(this);

    Wait(TEST_LINE_HEAT_CHAMBER_TEST_TIME); // Testing

    testLine->stat(testLine->heatChamber.Used());

    for (Queue::iterator q = testLine->heatChamberProducts.begin(); q != testLine->heatChamberProducts.end(); q++) {
        prod = (Product *) (*q);
        prod->Out(); // Remove from queue
        prod->Activate();
    }

    testLine->heatTest = NULL;

}