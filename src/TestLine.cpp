/*
 * File:   TestLine.cpp
 * Author: ondrej
 *
 * Created on 3. prosinec 2013, 20:44
 */

#include "TestLine.h"
#include "Storage.h"

TestLine *testLine;

TestLine::TestLine() {
    testers.SetCapacity(TEST_LINE_TESTER_COUNT);
    testers.SetName("Testeri");
    heatChamber.SetCapacity(TEST_LINE_HEAT_CHAMBER_CAPACITY);
    product = &storageTest;
    testedProduct = &storageFinal;
    heatChamberFree = true;
    heatChamber.SetName("Tepelna komora");
    stat.SetName("Vyuziti tepelne komory");
    heatTest = NULL;
}

void TestLine::EnterTestLine(Process *p, Entity::Priority_t priority) {
    EntityPriority_t oldP;
    oldP = p->Priority;
    p->Priority = priority;
    p->Enter(testers);
    p->Priority = oldP;
}

void TestLine::LeaveTestLine(Process *p) {
    p->Leave(testers);
}

void TestLine::DoTesting(Product *p) {

    product->fetch();

    double t = Time;
    p->Wait(TEST_LINE_TEST_TIME);
    p->productionTime += Time - t;

    if (Random() < TEST_LINE_TEST_FAIL_PROBABIBLITY) {
        p->Reset();
        LeaveTestLine(p);
        p->Terminate();
    }
}

bool TestLine::HeatChamberFree() {

    return heatChamberFree;

}

void TestLine::EnterHeatChamber(Product *p) {
    p->Enter(heatChamber);
    heatChamberFree = !heatChamber.Full();
    heatChamberProducts.Insert(p);
}

void TestLine::DoHeatChamberSetup(HeatChamberTest *t) {
    heatChamberFree = false;
    t->Wait(TEST_LINE_HEAT_CHAMBER_SETUP);
}

void TestLine::DoHeatChamberTest(Product *p) {
    if (heatTest == NULL) {
        heatTest = new HeatChamberTest();
        heatTest->Activate(Time + TEST_LINE_HEAT_CHAMBER_MAX_WAIT);
    }

    if (heatChamber.Full()) {
        heatTest->Activate();
    }

    double t = Time;
    p->Passivate();
    p->productionTime += Time - t;
}

void TestLine::DoHeatChamberAfterTest(Product *p) {
    p->Leave(heatChamber);
    heatChamberFree = heatChamber.Empty();
    double t = Time;
    p->Wait(TEST_LINE_HEAT_CHAMBER_AFTER_TEST_TIME);
    p->productionTime += Time - t;

    if (Random() < TEST_LINE_TEST_FAIL_PROBABIBLITY) {
        p->Reset();
        LeaveTestLine(p);
        p->Terminate();
    }
}

bool TestLine::IsFull(Product* p) {
    return product->IsFull();
}