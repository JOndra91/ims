/*
 * File:   IncomingOrderGenerator.cpp
 * Author: ondrej
 *
 * Created on 3. prosinec 2013, 11:32
 */

#include "IncomingOrderGenerator.h"
#include "IncomingOrder.h"
#include "Calendar.h"
#include "constants.h"
#include "IncomingDelivery.h"

void IncomingOrderGenerator::Behavior() {
    if (calendar.day <= 15) {
        (new IncomingOrder)->Activate();
        Activate(Time + ORDER_DELTA);
    }
}