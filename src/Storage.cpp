/*
 * File:   EmptyStore.cpp
 * Author: ondrej
 *
 * Created on 1. prosinec 2013, 12:08
 */

#include <string.h>
#include <stdio.h>
#include "Storage.h"
#include "constants.h"
#include "Product.h"

Storage storageV1("V1 Storage", V1_STORAGE_CAPACITY);
Storage storageV2("V2 Storage", V2_STORAGE_CAPACITY);
Storage storageTest("Test product storage", V_STORAGE_CAPACITY);
Storage storageFinal("Final product storage", OUT_STORAGE_CAPACITY);

Storage::Storage(unsigned long capacity) {
    s.SetCapacity(capacity);
    name[0] = 0;
    s.SetName(name);
    componentType = -1;
    qIn.SetName("Vstupni fronta skladiste");
    qOut.SetName("Vystupni fronta skladiste");
}

Storage::Storage(const char* name, unsigned long capacity, int componentType) {
    s.SetCapacity(capacity);
    setName(name, componentType);
    s.SetName(this->name);
    this->componentType = componentType;
    qIn.SetName("Vstupni fronta skladiste");
    qOut.SetName("Vystupni fronta skladiste");
}

void Storage::store(unsigned long amount) {
    bool inQueue = false;

    if (!qIn.Empty()) {
        qIn.Insert(Current);
        inQueue = true;
        ((Product*) Current)->LongWait();
    }

    while (1) {
        if (s.Free() >= amount) {

            if (inQueue) {
                qIn.Remove(Current);
            }

            s.Enter(Current, amount);

            if (!qOut.Empty()) {
                qOut.front()->Activate();
            }

            if (!qIn.Empty()) {
                qIn.front()->Activate();
            }

            return;
        }

        if (s.Free() > 0) {
            amount -= s.Free();
            s.Enter(Current, s.Free());
        }

        if (!qOut.Empty()) {
            qOut.front()->Activate();
        }

        if (!inQueue) {
            qIn.Insert(Current);
            inQueue = true;
        }

        ((Product*) Current)->LongWait();
    }
}

void Storage::fetch(unsigned long amount) {
    bool inQueue = false;

    if (!qOut.Empty()) {
        qOut.Insert(Current);
        inQueue = true;
        ((Product*) Current)->LongWait();
    }

    while (1) {
        if (s.Used() >= amount) {

            if (inQueue) {
                qOut.Remove(Current);
            }

            s.Leave(amount);

            if (!qIn.Empty()) {
                qIn.front()->Activate();
            }

            if (!qOut.Empty()) {
                qOut.front()->Activate();
            }

            return;
        }

        if (s.Used() > 0) {
            amount -= s.Used();
            s.Leave(s.Used());
        }

        if (!qIn.Empty()) {
            qIn.front()->Activate();
        }

        if (!inQueue) {
            qOut.Insert(Current);
            inQueue = true;
        }

        ((Product*) Current)->LongWait();
    }
}

void Storage::setName(const char* name, int index) {

    if (index >= 0) {
        snprintf(this->name, 64, "%s [%d]", name, index);
    } else {
        strncpy(this->name, name, 64);
    }
}