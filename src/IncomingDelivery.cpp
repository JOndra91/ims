/*
 * File:   IncomingDelivery.cpp
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 17:21
 */

#include "IncomingDelivery.h"
#include "Calendar.h"

// Only average times
double IncomingDelivery::checkTime[CombinedStorage::COMPONENT_COUNT] = {
    35 * Calendar::SECOND,
    10 * Calendar::SECOND,
    5.2 * Calendar::SECOND,
    15 * Calendar::SECOND,
    0.0 // No check
};

Store IncomingDelivery::inspector("Inspectors", INCOMING_DELIVERY_INSPECTOR_COUNT);

IncomingDelivery::IncomingDelivery(unsigned long quantity, int type, unsigned long extraQuantity, Priority_t p) : Process(p) {
    SetName("Prichozi objednavka");
    this->type = type;
    this->quantity = quantity;
    this->extraQuantity = extraQuantity;
}

void IncomingDelivery::Behavior() {
    IncomingDelivery *delivery;
    long multiplier;
    unsigned long i, total, extraOriginal, spareOrder;
    long spareParts, spareCapacity;
    double extraRate;
    long broken = 0;

    multiplier = CombinedStorage::multiplier[type];
    extraRate = CombinedStorage::extraRate[type];

    extraOriginal = extraQuantity;
    total = (quantity + extraQuantity);

    if (IncomingDelivery::checkTime[type] > 0.0) {
        Enter(inspector);
        Wait(IncomingDelivery::checkTime[type] * total); // Inspecting components

        for (i = 0; i < total; i++) {
            if (Random() < INCOMING_DELIVERY_CHECK_FAIL_PROBABILITY) {
                broken++; // Faulty component
            }
        }
        Leave(inspector);
    }

    internalStorage->getStorage(type)->store(quantity + extraQuantity - broken); // Store components
    internalStorage->spareComponents[type] -= broken;

    if (quantity > 0) {
        spareOrder = quantity * extraRate;

        spareParts = internalStorage->spareComponents[type];
        spareCapacity = internalStorage->getStorage(type)->Capacity() * extraRate;

        if (spareOrder + spareParts > spareCapacity) {
            spareOrder = 0;
        }

        internalStorage->spareComponents[type] += spareOrder;

        delivery = new IncomingDelivery(0, type, spareOrder);
        delivery->Activate(Time + INCOMING_DELIVERY_TIME);
    }
}
