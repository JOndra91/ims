/*
 * File:   Production1.h
 * Author: ondrej
 *
 * Created on 30. listopad 2013, 14:16
 */

#ifndef PRODUCTIONLINE2_H
#define	PRODUCTIONLINE2_H

class ProductionLine2;

extern ProductionLine2 *line2;

#include "simlib.h"
#include "constants.h"
#include "Product.h"
#include "Storage.h"

class ProductionLine2 {
public:
    ProductionLine2();
    void EnterLine(Product *p);
    void LeaveLine(Product *p);
    void DoProduction(Product *p);
    Storage *componentV1;
    Storage *componentB;
    Storage *componentC;
    Storage *componentE;
    Store line;
};

#endif	/* PRODUCTIONLINE2_H */

