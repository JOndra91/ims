/*
 * File:   Customer.cpp
 * Author: ondrej
 *
 * Created on 3. prosinec 2013, 17:55
 */

#include "Customer.h"
#include "Storage.h"
#include "constants.h"

Queue customerQueue("Customer queue");

unsigned long Customer::reserverdProducts = 0;

Customer::Customer(unsigned long ordered, Priority_t p) : Process(p) {
    this->ordered = ordered;
}

void Customer::Behavior() {
    Wait(CUSTOMER_WAIT_FOR_FETCH);

    storageFinal.fetch(ordered);
    Customer::reserverdProducts -= ordered;
}