/*
 * File:   Calendar.h
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 16:49
 */

#ifndef CALENDAR_H
#define	CALENDAR_H

class Calendar;

extern Calendar calendar;

#include "simlib.h"

#define CALENDAR_SECOND (1.0 / (60 * 60))
#define CALENDAR_MINUTE (CALENDAR_SECOND * 60.0)
#define CALENDAR_HOUR (CALENDAR_MINUTE * 60.0)
#define CALENDAR_DAY (CALENDAR_HOUR * 24.0)
#define CALENDAR_WEEK (CALENDAR_DAY * 7.0)

extern Histogram *histogramProductsPerDay;

class Calendar : public Process {
public:
    static unsigned long tempProducts;
    static const double SECOND = CALENDAR_SECOND;
    static const double MINUTE = CALENDAR_MINUTE;
    static const double HOUR = CALENDAR_HOUR;
    static const double DAY = CALENDAR_DAY;
    static const double WEEK = CALENDAR_WEEK;

    int month;
    int day;
    int phase;

    Calendar(Priority_t p = DEFAULT_PRIORITY);

    void Behavior();
};

#endif	/* CALENDAR_H */

