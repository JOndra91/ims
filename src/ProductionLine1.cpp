/*
 * File:   Production1.cpp
 * Author: ondrej
 *
 * Created on 30. listopad 2013, 14:16
 */

#include "ProductionLine1.h"
#include "constants.h"
#include "Product.h"
#include "CombinedStorage.h"

// Production line 1 initialization
ProductionLine1 *line1;

ProductionLine1::ProductionLine1() {
    int i;

    lineStat.SetName("Casové statistiky linky 1");
    used = 0;

    for (i = 0; i < PRODUCTION_1_LINE_COUNT; i++) {
        componentA[i] = (*internalStorage)[CombinedStorage::COMPONENT_A];
        componentE[i] = (*internalStorage)[CombinedStorage::COMPONENT_E];
        batchCounter[i] = 0;
        failureGenerator[i] = LineFailure1::Generator::create(i);
    }
}

void ProductionLine1::SeizeLine(Product* p) {
    int i;
    p->line = -1;

    do {
        for (i = 0; i < PRODUCTION_1_LINE_COUNT; i++) {
            if (!line[i].Busy()) {
                p->Seize(line[i]);
                lineStat(++used);
                p->line = i;
                break;
            }
        }

        if (p->line == -1) {
            lineQueue.Insert(p);
            Passivate(p);
        }
    } while (p->line == -1);
}

void ProductionLine1::ReleaseLine(Product *p) {

    p->Release(line[p->line]);
    lineStat(--used);
    lineStat.correction();

    if (!lineQueue.Empty()) {
        lineQueue.GetFirst()->Activate();
    }
}

void ProductionLine1::SeizeService(Product *p) {
    int i;
    p->service = -1;

    do {
        for (i = 0; i < PRODUCTION_1_SERVICE_COUNT; i++) {
            if (!service[i].Busy()) {
                p->Seize(service[i]);
                p->service = i;
                break;
            }
        }

        if (p->service == -1) {
            serviceQueue.Insert(p);
            p->LongWait();

            if (p->interrupted) {
                serviceQueue.Remove(p);
                (new Product())->Activate();
                p->Terminate();
            }

        }
    } while (p->service == -1);

}

Facility *ProductionLine1::SeizeService() {
    int i;
    Facility *f = NULL;
    Process *p = (Process*) Current;

    do {
        for (i = 0; i < PRODUCTION_1_SERVICE_COUNT; i++) {
            if (!service[i].Busy()) {
                f = service[i];
                p->Seize(*f);
                return f;
            }
        }

        if (f == NULL) {
            serviceQueue.Insert(Current);
            ((Process*) Current)->Passivate();
        }
    } while (f == NULL);
}

void ProductionLine1::ReleaseService(Product *p) {

    p->Release(service[p->service]);

    if (!serviceQueue.Empty()) {
        serviceQueue.GetFirst()->Activate();
    }
}

void ProductionLine1::ReleaseService(Facility* f) {

    ((Process*) Current)->Release(*f);

    if (!serviceQueue.Empty()) {
        serviceQueue.GetFirst()->Activate();
    }
}

void ProductionLine1::DoService(Product* p) {

    double t = Time;
    p->Wait(1 * Calendar::HOUR);
    p->productionTime += Time - t;

    if (p->interrupted) {
        ReleaseService(p);
        (new Product())->Activate();
        p->Terminate();
    }

    batchCounter[p->line] = PRODUCTION_1_BATCH_SIZE;
}

bool ProductionLine1::NeedsService(Product* p) {
    return (batchCounter[p->line] <= 0);
}

void ProductionLine1::DoProduction(Product* p) {
    batchCounter[p->line]--;

    // Take components
    componentA[p->line]->fetch();
    componentE[p->line]->fetch(3);

    if (p->interrupted) {
        componentA[p->line]->store();
        componentE[p->line]->store(3);
        (new Product())->Activate();
        p->Terminate();
    }

    p->componentsUsed[CombinedStorage::COMPONENT_A] += 1;
    p->componentsUsed[CombinedStorage::COMPONENT_E] += 3;

    p->startTime = Time;
    p->inProduction = true;

    p->Wait(PRODUCTION_1_PRODUCTION_TIME);
    p->productionTime += Time - p->startTime;

    p->inProduction = false;

    if (p->interrupted) {
        p->Reset();
        p->Terminate();
    }

    storageV1.store();
}