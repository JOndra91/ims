/*
 * File:   IncomingOrder.h
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 18:26
 */

#ifndef INCOMINGORDER_H
#define	INCOMINGORDER_H

#include "simlib.h"

class IncomingOrder : public Event {
public:
    void Behavior();
};

#endif	/* INCOMINGORDER_H */

