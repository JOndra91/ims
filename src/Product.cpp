/*
 * File:   Product.cpp
 * Author: ondrej
 *
 * Created on 29. listopad 2013, 19:36
 */

#include <math.h>

#include "Product.h"
#include "ProductionLine1.h"
#include "ProductionLine2.h"
#include "ProductionLine3.h"
#include "IncomingOrderGenerator.h"
#include "Customer.h"
#include "TestLine.h"
#include "constants.h"

Histogram *productHistogram;
Histogram *histogramInLine1;
Histogram *histogramInLine2;
Histogram *histogramInLine3;
Histogram *histogramInLine4;
Histogram *histogramInLine4Chamber;
Histogram *histogramProductsPerDay;
Histogram *histogramProductionTime;

unsigned long Product::products = 0;
unsigned long Product::productsToday = 0;
unsigned long Product::faultyProducts = 0;

double Product::timeEnd = 0.0;


int tempTime = 0, tempTime2 = 0;

Product::Product(Priority_t p) : Process(p) {
    SetName("Product");
    id = products++;
    for (int i = 0; i < CombinedStorage::COMPONENT_COUNT; i++) {
        componentsUsed[i] = 0;
    }
    seized = NULL;
    productionTime = 0;
    interrupted = false;
    inProduction = false;
};

void Product::Behavior() {

    /*
     * Linka 1
     *
     * Obsazení linky
     * Seřízení zařízení
     * Odebrat A+E
     * Výroba
     * Uskladnění výrobkuA
     * Uvolnění linky
     */

    line1->SeizeLine(this);
    // STAT in
    tempTime = Time;

    if (line1->NeedsService(this)) {
        line1->SeizeService(this);
        if (line1->NeedsService(this)) { // Services by another process?
            line1->DoService(this);
        }
        line1->ReleaseService(this);
    }
    line1->DoProduction(this);

    // STAT out
    (*histogramInLine1) (Time - tempTime); // save
    if (!this->interrupted) {
        line1->ReleaseLine(this);
    }


    /*
     * Linka 2
     *
     * Obsazení linky
     * Odebrat výrobek + B + C + 3E
     * Výroba
     * Uvolnění linky
     * Uskladnění výrobku
     */


    line2->EnterLine(this);
    tempTime = Time; // point

    line2->DoProduction(this);

    (*histogramInLine2) (Time - tempTime); // save
    line2->LeaveLine(this);


    /*
     * Linka 3
     *
     * Obsazení
     * Odebrat výrobek + D
     * Výroba
     * Sklad / Kontrola
     */

    line3->EnterLine(this);
    tempTime = Time; // point

    line3->DoProduction(this);

    if (testLine->IsFull(this)) {
        StoreFinal(); // Store for customer
        (*histogramInLine3) (Time - tempTime); // save
        line3->LeaveLine(this);
        Terminate();
    }

    storageTest.store(); // Store for test
    (*histogramInLine3) (Time - tempTime); // save
    line3->LeaveLine(this);



    /*
     * Kontrola
     *
     * Obsazení linky
     * Odebrat výrobek
     * Kontrola výrobku
     * Tepelná komora pokud je volná
     *  - Pokud je stále volná
     *   - Uvolnění linky
     *  - Pokud byla zaplněna
     *   - Nastavení tepelné zkoušky
     *   - Uvolnění linky
     *   - Tepelná zkouška
     *  - Zabrání linky s prioritou
     *  - Rychlá zkouška
     * Uskladnění
     * Uvolnění linky
     */


    testLine->EnterTestLine(this);
    tempTime = Time; // point

    testLine->DoTesting(this);
    if (testLine->HeatChamberFree()) {
        testLine->EnterHeatChamber(this);
        tempTime2 = Time; // point

        testLine->LeaveTestLine(this);
        testLine->DoHeatChamberTest(this);

        (*histogramInLine4Chamber) (Time - tempTime2); // save

        testLine->EnterTestLine(this, 1); // Test product from heat chamber with priority
        testLine->DoHeatChamberAfterTest(this);
    }

    StoreFinal(); // dal jsem to dovnitr, jako je to nahore, aby to bylo jednotne

    (*histogramInLine4) (Time - tempTime); // save
    testLine->LeaveTestLine(this);

}

void Product::StoreFinal() {
    Customer *customer;

    storageFinal.store();

    (*productHistogram)(Time - startTime);
    (*histogramProductionTime)(productionTime);
    productsToday++; // inkrementuji, kolik se vyrobilo za den
    (*histogramProductsPerDay) (calendar.day);

    customer = (Customer *) customerQueue.front();

    if (customer != NULL) {

        if (storageFinal.Stored() - Customer::reserverdProducts >= customer->ordered) {
            Customer::reserverdProducts += customer->ordered;
            customer->Out();
            customer->Activate();
        }
    }
}

void Product::Reset() {

    faultyProducts++;

    for (int i = 0; i < CombinedStorage::COMPONENT_COUNT; i++) {
        internalStorage->spareComponents[i] -= componentsUsed[i];
    }

    (new Product())->Activate();
}

void Product::LongWait() {
    Wait(1e27);
}
