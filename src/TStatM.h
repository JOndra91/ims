/*
 * File:   TStatM.h
 * Author: ondrej
 *
 * Created on 9. prosinec 2013, 21:13
 */

#ifndef TSTATM_H
#define	TSTATM_H

class TStatM;

#include "simlib.h"

class TStatM : public TStat {
public:
    void correction();
};

#endif	/* TSTATM_H */

