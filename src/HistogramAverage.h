/*
 * File:   HistogramAverage.h
 * Author: ondrej
 *
 * Created on 9. prosinec 2013, 13:05
 */

#ifndef HISTOGRAMAVERAGE_H
#define	HISTOGRAMAVERAGE_H

class HistogramAverage;

#include "simlib.h"

class HistogramAverage : public Histogram {
protected:
    unsigned div;
public:
    HistogramAverage(const char *_name, double low, double step, unsigned count, unsigned divide);
    void Output();
};

#endif	/* HISTOGRAMAVERAGE_H */

