/*
 * File:   ProductionLine3.h
 * Author: ondrej
 *
 * Created on 4. prosinec 2013, 20:55
 */

#ifndef PRODUCTIONLINE3_H
#define	PRODUCTIONLINE3_H

class ProductionLine3;

extern ProductionLine3 *line3;

#include "constants.h"
#include "Product.h"
#include "Storage.h"

class ProductionLine3 {
public:
    ProductionLine3();
    void EnterLine(Product *p);
    void LeaveLine(Product *p);
    void DoProduction(Product *p);
    Storage *componentV2;
    Storage *componentD;
    Store line;
};

#endif	/* PRODUCTIONLINE3_H */

