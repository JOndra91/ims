/*
 * File:   FakeQueue.cpp
 * Author: ondrej
 *
 * Created on 5. prosinec 2013, 22:12
 */

#include <string.h>
#include <stdio.h>

#include "FakeQueue.h"

FakeQueue::FakeQueue() {
    snprintf(name, NAME_LEN, "Queue{%p}", this);
}

FakeQueue::FakeQueue(const char* name) {
    SetName(name);
}

void FakeQueue::SetName(const char* name) {
    strncpy(this->name, name, NAME_LEN);
}

const char *FakeQueue::Name() {
    return name;
}

void FakeQueue::Output() {
    char s[100];
    Print("+----------------------------------------------------------+\n");
    Print("| QUEUE %-39s %10s |\n", Name(), StatN.Number() ? "" : "not used");
    if (StatN.Number() > 0) {
        Print("+----------------------------------------------------------+\n");
        sprintf(s, " Time interval = %g - %g ", StatN.StartTime(), (double) Time);
        Print("| %-56s |\n", s);
        Print("|  Incoming  %-26ld                    |\n", StatN.Number());
        Print("|  Outcoming  %-26ld                   |\n", StatDT.Number());
        Print("|  Current length = %-26lu             |\n", size());
        Print("|  Maximal length = %-25g              |\n", StatN.Max());
        double dt = double(Time) - StatN.StartTime();
        if (dt > 0) {
            double mv = StatN.MeanValue();
            Print("|  Average length = %-25g              |\n", mv);
        }
        if (StatDT.Number() > 0) {
            Print("|  Minimal time = %-25g                |\n", StatDT.Min());
            Print("|  Maximal time = %-25g                |\n", StatDT.Max());
            double mv = StatDT.MeanValue();
            Print("|  Average time = %-25g                |\n", mv);
            if (StatDT.Number() > 99)
                Print("|  Standard deviation = %-25g          |\n",
                    StatDT.StdDev());
        }
    }
    Print("+----------------------------------------------------------+\n");

}

void FakeQueue::Insert(Entity* e) {

    iterator p = end();
    Entry *en;

    while (p != begin()) {
        --p;
        en = (Entry*) (*p);
        if (e->Priority <= en->entity->Priority) {
            ++p;
            break;
        }
    }

    list.insert(p, new Entry(e));

    //    StatN(size()); // length statistic

    StatN(size());
}

void FakeQueue::Remove(Entity * e) {

    //    StatDT(double(Time) - ent->_MarkTime);
    //    StatN(size());
    //    StatN.n--; // correction !!!

    Entry *ent;

    FakeQueue::iterator p = find(e);

    if (p == end()) {
        return;
    }

    ent = ((Entry*) (*p));
    list.erase(p);

    StatDT(double(Time) - ent->markTime);

    delete ent;

    StatN(size());
    StatN.correction();
}

void FakeQueue::Remove(iterator pos) {

    //    StatDT(double(Time) - ent->_MarkTime);
    //    StatN(size());
    //    StatN.n--; // correction !!!

    Entry *ent;

    ent = ((Entry*) (*pos));
    list.erase(pos);

    StatDT(double(Time) - ent->markTime);

    delete ent;

    StatN(size());
    StatN.correction();
}

bool FakeQueue::HasEntity(Entity* e) {
    return find(e) != end();
}

Entity * FakeQueue::GetFirst() {
    return Get(begin());
}

FakeQueue::iterator FakeQueue::find(Entity* e) {
    FakeQueue::iterator p = begin();

    for (; p != end(); ++p) {
        if (((Entry*) * p)->entity == e) {
            break;
        }
    }
    return p;
}

FakeQueue::iterator FakeQueue::begin() {
    return list.begin();
}

FakeQueue::iterator FakeQueue::end() {
    return list.end();
}

Entity *FakeQueue::front() {

    if (list.empty()) {
        return NULL;
    }

    FakeQueue::iterator p = begin();

    return ((Entry*) (*begin()))->entity;
}

Entity *FakeQueue::Get(FakeQueue::iterator pos) {

    Entity *e;
    Entry *ent;

    ent = (Entry*) (*pos);

    e = ent->entity;
    Remove(pos);

    return e;

}

FakeQueue::Entry::Entry(Entity* e) {
    entity = e;

    //    ent->_MarkTime = Time; // marks input time
    markTime = double(Time);
}