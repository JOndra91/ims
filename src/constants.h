/*
 * File:   constants.h
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 18:30
 */

#ifndef CONSTANTS_H
#define	CONSTANTS_H

#include "simlib.h"
#include "Calendar.h"

#define NUMBER_OF_MONTH 36

#define ORDER_QUANTITY Uniform(2600, 3750)
#define ORDER_DELTA ((Exponential(3.5) + Normal(5.0, 1.0))*Calendar::HOUR)
#define ORDER_SPARE_COMPONENTS (0.10) // 10% of spare components

#define INCOMING_DELIVERY_TIME Normal((6*Calendar::DAY), (1*Calendar::DAY))
#define INCOMING_DELIVERY_CHECK_FAIL_PROBABILITY (0.012)
#define INCOMING_DELIVERY_INSPECTOR_COUNT 10 // 10

#define INTERNAL_STORAGE_CAPACITY 8000
#define V1_STORAGE_CAPACITY 2000
#define V2_STORAGE_CAPACITY 2000
#define V_STORAGE_CAPACITY 2000
#define OUT_STORAGE_CAPACITY 8000

#define PRODUCTION_1_LINE_COUNT 22 // 22
#define PRODUCTION_1_SERVICE_COUNT 5
#define PRODUCTION_1_BATCH_SIZE 80
#define PRODUCTION_1_PRODUCTION_TIME (Calendar::HOUR/12.18) // 12.18 pcs/hour
#define PRODUCTION_1_SERVICE_TIME (10*Calendar::MINUTE)
#define PRODUCTION_1_FAILURE_SCATTER ((Exponential(120.0))*Calendar::WEEK)
#define PRODUCTION_1_FAILURE_FIX (Exponential(10.0*Calendar::MINUTE) + Normal(30.0*Calendar::MINUTE, 5.0*Calendar::MINUTE))


#define PRODUCTION_2_LINE_COUNT 25 // 20
#define PRODUCTION_2_PRODUCTION_TIME (Calendar::HOUR/12) // 12 pcs/hour

#define PRODUCTION_3_LINE_COUNT 37 // 37
#define PRODUCTION_3_PRODUCTION_TIME (Calendar::HOUR/7) // 7 pcs hour

#define TEST_LINE_TESTER_COUNT 15 // 15
#define TEST_LINE_HEAT_CHAMBER_CAPACITY 30
#define TEST_LINE_TEST_FAIL_PROBABIBLITY 0.007
#define TEST_LINE_TEST_TIME Normal(12.5*Calendar::MINUTE, (2.5/3)*Calendar::MINUTE) // 10 - 15 Minutes
#define TEST_LINE_HEAT_CHAMBER_SETUP (5*Calendar::MINUTE)
#define TEST_LINE_HEAT_CHAMBER_MAX_WAIT (30*Calendar::MINUTE)
#define TEST_LINE_HEAT_CHAMBER_TEST_TIME (2*Calendar::HOUR)
#define TEST_LINE_HEAT_CHAMBER_AFTER_TEST_TIME Normal(5*Calendar::MINUTE, 1*Calendar::MINUTE) // 2 - 8 Minutes

#define CUSTOMER_WAIT_FOR_FETCH Uniform(3*Calendar::HOUR, 6*Calendar::HOUR)

#endif	/* CONSTANTS_H */

