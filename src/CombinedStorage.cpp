/*
 * File:   ExternalStorage.cpp
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 17:04
 */

#include "CombinedStorage.h"
#include <stdio.h>

// Internal storage initialization
CombinedStorage *internalStorage;

double CombinedStorage::extraRate[] = {
    0.1, 0.1, 0.1, 0.1, 0.05
};

long CombinedStorage::multiplier[] = {
    1, 1, 1, 1, 4
};

CombinedStorage::CombinedStorage(const char *name, unsigned long capacity) {
    int i, mul;

    for (i = 0; i < CombinedStorage::COMPONENT_COUNT; i++) {
        stores[i] = new Storage(name, capacity * multiplier[i], i);
        spareComponents[i] = 0;
    }
}

Storage* CombinedStorage::getStorage(int type) {
    return stores[type];
}

Storage* CombinedStorage::operator [](int type) {
    return getStorage(type);
}

void CombinedStorage::Output() {
    int i;

    for (i = 0; i < CombinedStorage::COMPONENT_COUNT; i++) {
        stores[i]->Output();
    }
}