/*
 * File:   IncomingOrder.cpp
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 18:26
 */

#include "constants.h"
#include "IncomingOrder.h"
#include "Calendar.h"
#include "IncomingDelivery.h"
#include "CombinedStorage.h"
#include "Product.h"
#include "Customer.h"

void IncomingOrder::Behavior() {
    Product *product;
    Customer *customer;
    IncomingDelivery *delivery;
    int i, multiplier;
    unsigned long quantity, totalOrder;

    Priority_t p = DEFAULT_PRIORITY;
    if (Random() < 0.1) { // 10% chance of order with higher priority
        p = 1;
    }

    quantity = (unsigned long) ORDER_QUANTITY;

    for (i = 0; i < CombinedStorage::COMPONENT_COUNT; i++) {
        // Generate deliveries of different components with different delays
        multiplier = CombinedStorage::multiplier[i];

        totalOrder = quantity * multiplier;

        delivery = new IncomingDelivery(totalOrder, i, 0);
        delivery->Activate(Time + INCOMING_DELIVERY_TIME);
    }

    for (i = 0; i < quantity; i++) {
        product = new Product();
        product->Activate();
    }

    customer = new Customer(quantity, p);
    customerQueue.Insert(customer);
}

