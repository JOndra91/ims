/*
 * File:   IncomingDelivery.h
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 17:21
 */

#ifndef INCOMINGDELIVERY_H
#define	INCOMINGDELIVERY_H

#include "simlib.h"
#include "CombinedStorage.h"
#include "constants.h"

class IncomingDelivery : public Process {
    unsigned long quantity;
    unsigned long extraQuantity;
    int type;
    static double checkTime[CombinedStorage::COMPONENT_COUNT];
public:
    static Store inspector;
    IncomingDelivery(unsigned long quantity, int type, unsigned long extraQuantity, Priority_t p = DEFAULT_PRIORITY);
    void Behavior();
};

#endif	/* INCOMINGDELIVERY_H */

