/*
 * File:   LineFailure1.cpp
 * Author: ondrej
 *
 * Created on 5. prosinec 2013, 18:03
 */

#include "LineFailure1.h"
#include "constants.h"
#include "ProductionLine1.h"

LineFailure1::LineFailure1(int line) : Process() {
    this->lineIndex = line;
    Priority = 1;
}

void LineFailure1::Behavior() {

    Facility *f, *s;
    Product *p;

    f = line1->line[lineIndex];

    Seize(*f, 1);

    if (!f->Q2->Empty()) {
        p = (Product*) f->Q2->GetFirst();
        p->interrupted = true;
        line1->lineStat(--(line1->used));
        line1->lineStat.correction();
        if (p->inProduction) {
            p->Activate(); // Will be activated only when line is running (in Wait)
        }
    }

    s = line1->SeizeService();
    line1->batchCounter[lineIndex] = 0;
    Wait(PRODUCTION_1_FAILURE_FIX);
    line1->ReleaseService(s);

    Release(*f);

}

LineFailure1::Generator::Generator(int line) : Event() {
    this->line = line;
}

void LineFailure1::Generator::Behavior() {

    (new LineFailure1(line))->Activate();
    Activate(Time + PRODUCTION_1_FAILURE_SCATTER);

}

LineFailure1::Generator *LineFailure1::Generator::create(int line) {
    LineFailure1::Generator *generator;

    generator = new LineFailure1::Generator(line);

    generator->Activate(Time + PRODUCTION_1_FAILURE_SCATTER);

    return generator;
}