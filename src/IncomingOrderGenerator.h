/*
 * File:   IncomingOrderGenerator.h
 * Author: ondrej
 *
 * Created on 3. prosinec 2013, 11:32
 */

#ifndef INCOMINGORDERGENERATOR_H
#define	INCOMINGORDERGENERATOR_H

class IncomingOrderGenerator;

#include "simlib.h"

class IncomingOrderGenerator : public Event {
public:
    void Behavior();
};

#endif	/* INCOMINGORDERGENERATOR_H */

