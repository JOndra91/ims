/*
 * File:   FakeQueue.h
 * Author: ondrej
 *
 * Created on 5. prosinec 2013, 22:12
 */

#ifndef FAKEQUEUE_H
#define	FAKEQUEUE_H

class FakeQueue;

#include <stdlib.h>
#include "simlib.h"
#include "TStatM.h"

class FakeQueue {
protected:
    static const int NAME_LEN = 128;

    class Entry {
    public:
        Entity *entity;
        double markTime;
        Entry(Entity *e);
    };
    std::list<Entry*> list;
    char name[NAME_LEN];
public:
    typedef std::list<Entry*>::iterator iterator;
    FakeQueue();
    FakeQueue(const char* name);
    TStatM StatN;
    Stat StatDT; // statistics
    void Output();
    void Insert(Entity *e);
    void Remove(Entity *e);
    void Remove(iterator pos);
    void SetName(const char* name);
    bool HasEntity(Entity *e);
    Entity *GetFirst();
    iterator begin();
    iterator end();
    iterator find(Entity *e);
    Entity *front();
    Entity *Get(iterator pos);
    const char* Name();

    bool Empty() {
        return list.empty();
    };

    unsigned size() {
        return list.size();
    }
};

#endif	/* FAKEQUEUE_H */

