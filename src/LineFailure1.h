/*
 * File:   LineFailure1.h
 * Author: ondrej
 *
 * Created on 5. prosinec 2013, 18:03
 */

#ifndef LINEFAILURE1_H
#define	LINEFAILURE1_H

class LineFailure1;

#include "simlib.h"

class LineFailure1 : public Process {
public:
    int lineIndex;
    LineFailure1(int line);
    void Behavior();

    class Generator : public Event {
    public:
        int line;
        void Behavior();
        static Generator *create(int line);
    protected:
        Generator(int line);
    };

};

#endif	/* LINEFAILURE1_H */

