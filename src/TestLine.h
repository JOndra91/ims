/*
 * File:   TestLine.h
 * Author: ondrej
 *
 * Created on 3. prosinec 2013, 20:44
 */

#ifndef TESTLINE_H
#define	TESTLINE_H

class TestLine;

#include "simlib.h"
#include "Product.h"
#include "constants.h"
#include "HeatChamberTest.h"

extern TestLine *testLine;

class TestLine {
public:
    Stat stat;
    TestLine();
    void EnterTestLine(Process *p, Entity::Priority_t priority = DEFAULT_PRIORITY);
    void LeaveTestLine(Process *p);
    void DoTesting(Product *p);
    bool HeatChamberFree();
    void EnterHeatChamber(Product *p);
    void DoHeatChamberSetup(HeatChamberTest *p);
    void DoHeatChamberTest(Product *p);
    void DoHeatChamberAfterTest(Product *p);
    bool IsFull(Product *p);

    Storage *product;
    Storage *testedProduct;
    Store testers;
    Store heatChamber;
    Queue heatChamberProducts;
    HeatChamberTest *heatTest;
    bool heatChamberFree;
};

/*
 * Kontrola
 *
 * Obsazení linky
 * Odebrat výrobek
 * Kontrola výrobku
 * Tepelná komora pokud je volná
 *  - Pokud je stále volná
 *   - Uvolnění linky
 *  - Pokud byla zaplněna
 *   - Nastavení tepelné zkoušky
 *   - Uvolnění linky
 *   - Tepelná zkouška
 *  - Zabrání linky s prioritou
 *  - Rychlá zkouška
 * Uskladnění
 * Uvolnění linky
 */

#endif	/* TESTLINE_H */

