/*
 * File:   Production1.h
 * Author: ondrej
 *
 * Created on 30. listopad 2013, 14:16
 */

#ifndef PRODUCTIONLINE1_H
#define	PRODUCTIONLINE1_H

class ProductionLine1;

extern ProductionLine1 *line1;

#include "simlib.h"
#include "constants.h"
#include "Product.h"
#include "Storage.h"
#include "LineFailure1.h"

class ProductionLine1 {
public:
    int used;
    TStatM lineStat;
    ProductionLine1();
    void SeizeLine(Product *p);
    void ReleaseLine(Product *p);
    void SeizeService(Product *p);
    Facility *SeizeService();
    void ReleaseService(Product *p);
    void ReleaseService(Facility *f);
    bool NeedsService(Product *p);
    void DoProduction(Product *p);
    void DoService(Product *p);
    Storage *componentA[PRODUCTION_1_LINE_COUNT];
    Storage *componentE[PRODUCTION_1_LINE_COUNT];
    Facility line[PRODUCTION_1_LINE_COUNT];
    Facility service[PRODUCTION_1_SERVICE_COUNT];
    int batchCounter[PRODUCTION_1_LINE_COUNT];
    Queue lineQueue;
    FakeQueue serviceQueue;
    Stat stat;
    LineFailure1::Generator *failureGenerator[PRODUCTION_1_LINE_COUNT];
};

#endif	/* PRODUCTIONLINE1_H */

