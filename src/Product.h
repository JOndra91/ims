/*
 * File:   Product.h
 * Author: ondrej
 *
 * Created on 29. listopad 2013, 19:36
 */

#ifndef PRODUCT_H
#define	PRODUCT_H

class Product;

#include "simlib.h"
#include "CombinedStorage.h"

extern Histogram *productHistogram;
extern Histogram *histogramInLine1;
extern Histogram *histogramInLine2;
extern Histogram *histogramInLine3;
extern Histogram *histogramInLine4;
extern Histogram *histogramInLine4Chamber;
extern Histogram *histogramProductsPerDay;
extern Histogram *histogramProductionTime;

class Product : public Process {
public:
    static double timeEnd;
    static unsigned long productsToday;
    static unsigned long products;
    static unsigned long faultyProducts;
    int id;
    bool interrupted;
    bool inProduction;
    unsigned long componentsUsed[CombinedStorage::COMPONENT_COUNT];
    double startTime;
    double productionTime;

    Product(Priority_t p = DEFAULT_PRIORITY);
    void Behavior();
    void StoreFinal();
    void Reset();
    void LongWait(); // SIMLIB Hack
    int service;
    int line;
    Facility *seized;
};

#endif	/* PRODUCT_H */

