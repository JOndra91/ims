/*
 * File:   EmptyStore.h
 * Author: ondrej
 *
 * Created on 1. prosinec 2013, 12:08
 */

#ifndef STORAGE_H
#define	STORAGE_H

class Storage;

extern Storage storageV1;
extern Storage storageV2;
extern Storage storageTest;
extern Storage storageFinal;

#include "simlib.h"
#include "FakeQueue.h"

class Storage {
    Store s;
    FakeQueue qIn, qOut;
    char name[64];
    int type;
    int componentType;
public:
    Storage(unsigned long capacity);
    Storage(const char *name, unsigned long capacity, int index = -1);
    void store(unsigned long amount = 1);
    void fetch(unsigned long amount = 1);
    void setName(const char *name, int index = -1);

    void setType(int type) {
        this->type = type;
    }

    int getComponentType() {
        return this->componentType;
    }

    void setComponentType(int type) {
        this->componentType = type;
    }

    unsigned long Free() {
        return s.Free();
    };

    unsigned long Stored() {
        return s.Used();
    };

    unsigned long Capacity() {
        return s.Capacity();
    };

    bool IsFull() {
        return s.Capacity() == s.Used();
    }

    void Output() {
        s.Output();
        qIn.Output();
        qOut.Output();
    }
};

#endif	/* STORAGE_H */

