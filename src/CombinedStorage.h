/*
 * File:   ExternalStorage.h
 * Author: ondrej
 *
 * Created on 27. listopad 2013, 17:04
 */

#ifndef COMBINEDSTORAGE_H
#define	COMBINEDSTORAGE_H

class CombinedStorage;

extern CombinedStorage *internalStorage;

#include "simlib.h"
#include "constants.h"
#include "Storage.h"

class CombinedStorage {
public:
    // Component types
    static const int COMPONENT_A = 0;
    static const int COMPONENT_B = 1;
    static const int COMPONENT_C = 2;
    static const int COMPONENT_D = 3;
    static const int COMPONENT_E = 4;
    static const int COMPONENT_COUNT = 5;
    static double extraRate[CombinedStorage::COMPONENT_COUNT];
    static long multiplier[CombinedStorage::COMPONENT_COUNT];
    unsigned long spareComponents[CombinedStorage::COMPONENT_COUNT];
private:
    Storage *stores[CombinedStorage::COMPONENT_COUNT];
public:
    CombinedStorage(const char *name, unsigned long capacity);
    Storage* getStorage(int type);
    Storage* operator[](int type);
    void Output();
};

#endif	/* STORAGE_H */

