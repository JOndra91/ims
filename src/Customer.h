/*
 * File:   Customer.h
 * Author: ondrej
 *
 * Created on 3. prosinec 2013, 17:55
 */

#ifndef CUSTOMER_H
#define	CUSTOMER_H

class Customer;

#include "simlib.h"

extern Queue customerQueue;

class Customer : public Process {
public:
    unsigned long ordered;
    static unsigned long reserverdProducts;
    Customer(unsigned long ordered, Priority_t p = DEFAULT_PRIORITY);
    void Behavior();
};

#endif	/* CUSTOMER_H */

