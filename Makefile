MAKEFILE=Makefile.$(shell uname -s)-$(shell uname -m)
NAME=ims

CC=g++

OBJ=main.o Calendar.o CombinedStorage.o Storage.o IncomingDelivery.o IncomingOrder.o Product.o ProductionLine1.o \
    IncomingOrderGenerator.o ProductionLine2.o Customer.o TestLine.o ProductionLine3.o HeatChamberTest.o LineFailure1.o \
    FakeQueue.o HistogramAverage.o TStatM.o
LDFLAGS=-l:lib/libsimlib.so.0
CFLAGS=-g
DOC=dokumentace.tex
PDF=dokumentace.pdf
BUILDDIR=build
SRCDIR=src
ARCH=03_xjanos12_xkadle29.tar.gz

RM=rm -rf

.PHONY: all clean run doc pack clean-run

$(NAME): $(addprefix $(BUILDDIR)/,$(OBJ))
	$(CC) $+ $(CFLAGS) -o $@ $(LDFLAGS)

$(BUILDDIR)/%.o: $(SRCDIR)/%.cpp | $(BUILDDIR)
	$(CC) -c $(CFLAGS) -o $@ $<

$(BUILDDIR):
	mkdir $(BUILDDIR)

all: clean $(NAME) doc pack

run:	$(NAME)
	./$(NAME)

clean-run: clean run

clean:
	$(RM) *.~ $(NAME) doc/statistiky.txt doc/$(PDF) $(BUILDDIR)

doc:
	cd doc/latex/; pdflatex $(DOC); mv -f $(PDF) ../$(PDF)

pack: $(NAME) run #doc pravidlo chybi kvuli bibliografickym citacim
	cp doc/statistiky.txt doc/statistiky_predgenerovane.txt
	tar -cvzf $(ARCH) $(SRCDIR)/*.cpp $(SRCDIR)/*.h Makefile doc/statistiky_predgenerovane.txt lib/libsimlib.so.0 doc/$(PDF)